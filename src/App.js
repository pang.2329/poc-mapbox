/* src/App.js */
import React, { useRef, useEffect } from 'react';
import ReactDOM from 'react-dom';
import mapboxgl from 'mapbox-gl';

import fetchFakeData from './api/fetchFakeData';
import Popup from './components/Popup';
import Marker from './components/Marker';
import IndoorMap from './components/IndoorMap';
import Indoor from './components/Indoor';


import './App.css';
import geoJson from './indoor_map.geojson';

mapboxgl.accessToken = 'pk.eyJ1IjoicGFuZ3RoaWRhcmF0IiwiYSI6ImNrZjdoaG84YTAyaDYyd280eTEwdWdsNngifQ.KDGMMalBRx4thNoYxw2Udg';


const App = () => {
  const mapContainerRef = useRef(null);
  const mapboxgl_indoor = new IndoorMap();


  // initialize map when component mounts
  useEffect(() => {
    const map = new mapboxgl.Map({
        container: "map",
        center: [-87.61694, 41.86625],
        zoom: 15.99,
        // zoom: 18,
        // center: [2.3592843, 48.8767904],
        pitch: 20,
        bearing: 20,
        antialias: true,
        pitchWithRotate: false,
        style: 'mapbox://styles/pangthidarat/ckfg119h00evk19oc5s4i2nmj',

        // style: 'mapbox://styles/mapbox/dark-v10',
        // source : {
        //   'type' : 'vector',
        //   'url' : 'mapbox://pangthidarat.a868gqg8'
        // }
    });
    map.on('load', function () {
       // Define a source before using it to create a new layer
      map.addSource('floorplan', {
        type: 'geojson',
        // url: 'mapbox://mapbox.mapbox-streets-v8'
        // data: './indoor_map.geojson',
        'data': 'https://docs.mapbox.com/mapbox-gl-js/assets/indoor-3d-map.geojson'
      });
      // Indoor.addMap(mapboxgl_indoor.fromGeojson(geoJson));
      map.addLayer({
        id: 'room-label',
        // References the GeoJSON source defined above
        // and does not require a `source-layer`
        source: 'floorplan',
        type: 'symbol',
        layout: {
        // Set the label content to the
        // feature's `name` property
        'text-field': ['get', 'name']
        }
      });
      map.addLayer({
        id: 'room-extrusion',
        type: 'fill-extrusion',
        source: 'floorplan',
        paint: {
          // See the Mapbox Style Specification for details on data expressions.
          // https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions
          
          // Get the fill-extrusion-color from the source 'color' property.
          'fill-extrusion-color': ['get', 'color'],
          
          // Get fill-extrusion-height from the source 'height' property.
          'fill-extrusion-height': ['get', 'height'],
          
          // Get fill-extrusion-base from the source 'base_height' property.
          'fill-extrusion-base': ['get', 'base_height'],
          
          // Make extrusions slightly opaque for see through indoor walls.
          'fill-extrusion-opacity': 0.5
        }
      });

    })



   
    // Retrieve the geojson from the path and add the map
    // fetch('./indoor_map.geojson')
    // .then(res => res.json())
    // .then(geojson => {
    //     map.indoor.addMap(mapboxgl_indoor.fromGeojson(geojson));
    // });
    // map.indoor.addMap(mapboxgl_indoor.fromGeojson(geoJson));

   

     

    // clean up on unmount
    return () => map.remove();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return <div className="map-container" ref={mapContainerRef} />;
};

export default App;
