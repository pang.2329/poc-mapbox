import { distance } from '@turf/distance';

import IndoorControl from './IndoorControl';
import { overlap, filterWithLevel } from './Utils';

const SOURCE_ID = 'indoor';

class Indoor {

    constructor(map) {
        this._map = map;
        this._level = null;

        this._indoorMaps = [];
        this._savedFilters = [];
        this._selectedMap = null;
        this._previousSelectedMap = null;
        this._previousSelectedLevel = null;
        this._mapLoaded = false;

        this._control = new IndoorControl(this);

        if (this._map.loaded()) {
            this._mapLoaded = true;
        } else {
            this._map.on('load', () => {
                this._mapLoaded = true;
                this._updateSelectedMapIfNeeded();
            });
        }

        this._map.on('moveend', () => this._updateSelectedMapIfNeeded());
    }

    getSelectedMap() {
        return this._selectedMap;
    }

    getLevel() {
        return this._level;
    }

    setLevel(level) {
        this._level = level;
        this._updateFiltering();
        this._map.fire('indoor.level.changed', { level });
    }

    get control() {
        return this._control;
    }

    /**
     * ***********************
     * Handle level change
     * ***********************
     */

    _addLayerForFiltering(layer, beforeLayerId) {
        this._map.addLayer(layer, beforeLayerId);
        this._savedFilters.push({
            layerId: layer.id,
            filter: this._map.getFilter(layer.id) || ["all"]
        });
    }

    _removeLayerForFiltering(layerId) {
        this._savedFilters = this._savedFilters.filter(({ layerId: id }) => layerId !== id);
        this._map.removeLayer(layerId);
    }

    _updateFiltering() {
        const level = this._level;

        let filterFn;
        if (level !== null) {
            const showFeaturesWithEmptyLevel = this._selectedMap ? this._selectedMap.showFeaturesWithEmptyLevel : false;
            filterFn = (filter) => filterWithLevel(filter, level, showFeaturesWithEmptyLevel);
        } else {
            filterFn = (filter) => filter;
        }

        this._savedFilters.forEach(({ layerId, filter }) => this._map.setFilter(layerId, filterFn(filter)));
    }



    /**
     * **************
     * Handle maps
     * **************
     */

    addMap(map) {
        this._indoorMaps.push(map);
        this._updateSelectedMapIfNeeded();
    }

    removeMap(map) {
        this._indoorMaps = this._indoorMaps.filter(_indoorMap => _indoorMap !== map);
        this._updateSelectedMapIfNeeded();
    }


    _updateSelectedMapIfNeeded() {

        if (!this._mapLoaded) {
            return;
        }

        const closestMap = this.closestMap();
        if (closestMap !== this._selectedMap) {
            this._updateSelectedMap(closestMap);
        }
    }

    _updateSelectedMap(indoorMap) {

        const previousMap = this._selectedMap;
        const previousMapLevel = this._level;
        this._selectedMap = indoorMap;

        // Remove the previous selected map if it exists
        if (previousMap !== null) {
            previousMap.layersToHide.forEach(layerId => this._map.setLayoutProperty(layerId, 'visibility', 'visible'));
            previousMap.layers.forEach(({ id }) => this._removeLayerForFiltering(id));
            this._map.removeSource(SOURCE_ID);
        }

        if (!indoorMap) {
            // Save the previous map level.
            // It enables the user to exit and re-enter, keeping the same level shown.
            this._previousSelectedLevel = previousMapLevel;
            this._previousSelectedMap = previousMap;

            this.setLevel(null);

            this._map.fire('indoor.map.unloaded', { indoorMap: previousMap });
            return;
        }


        const { geojson, layers, levelsRange, beforeLayerId } = indoorMap;

        // Add map source
        this._map.addSource(SOURCE_ID, {
            type: "geojson",
            data: geojson
        });

        // Add layers and save filters
        layers.forEach(layer => this._addLayerForFiltering(layer, beforeLayerId));

        // Hide layers which can overlap for rendering
        indoorMap.layersToHide.forEach(layerId => this._map.setLayoutProperty(layerId, 'visibility', 'none'));

        this._map.fire('indoor.map.loaded', { indoorMap });

        // Restore the same level when the previous selected map is the same.
        const level = this._previousSelectedMap === indoorMap
            ? this._previousSelectedLevel
            : Math.max(Math.min(indoorMap.defaultLevel, levelsRange.max), levelsRange.min)

        this.setLevel(level);
    }

    closestMap() {

        // TODO enhance this condition
        if (this._map.getZoom() < 17) {
            return null;
        }

        const cameraBounds = this._map.getBounds();
        const cameraBoundsTurf = [
            cameraBounds.getWest(),
            cameraBounds.getSouth(),
            cameraBounds.getEast(),
            cameraBounds.getNorth()
        ];

        const mapsInBounds = this._indoorMaps.filter(indoorMap =>
            overlap(indoorMap.bounds, cameraBoundsTurf)
        );

        if (mapsInBounds.length === 0) {
            return null;
        }

        if (mapsInBounds.length === 1) {
            return mapsInBounds[0];
        }

        /*
         * If there is multiple maps at this step, select the closest
         */
        let minDist = Number.POSITIVE_INFINITY;
        let closestMap = mapsInBounds[0];
        for (const map of mapsInBounds) {
            const _dist = distance(map.bounds.getCenter(), cameraBounds.getCenter());
            if (_dist < minDist) {
                closestMap = map;
                minDist = _dist;
            }
        }
        return closestMap;
    }

}

export default Indoor;