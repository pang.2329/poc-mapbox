import Style from './style';
import GeoJsonHelper from './GeoJsonHelper';

class IndoorMap {

    fromGeojson(geojson, options = {}) {

        const { bounds, levelsRange } = GeoJsonHelper.extractLevelsRangeAndBounds(geojson);

        const map = new IndoorMap();
        map.geojson = geojson;
        map.layers = options.layers ? options.layers : Style.DefaultLayers;
        map.bounds = bounds;
        map.levelsRange = levelsRange;
        map.layersToHide = options.layersToHide ? options.layersToHide : [];
        map.beforeLayerId = options.beforeLayerId;
        map.defaultLevel = options.defaultLevel ? options.defaultLevel : 0;
        map.showFeaturesWithEmptyLevel = options.showFeaturesWithEmptyLevel ? options.showFeaturesWithEmptyLevel : false;

        return map;
    }

}

export default IndoorMap;