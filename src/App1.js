/* src/App.js */
import React, { useRef, useEffect } from 'react';
import ReactDOM from 'react-dom';
import mapboxgl from 'mapbox-gl';

import fetchFakeData from './api/fetchFakeData';
import Popup from './components/Popup';
import Marker from './components/Marker';

import './App.css';

mapboxgl.accessToken = 'pk.eyJ1IjoicGFuZ3RoaWRhcmF0IiwiYSI6ImNrZjdoaG84YTAyaDYyd280eTEwdWdsNngifQ.KDGMMalBRx4thNoYxw2Udg';


const App = () => {
  const mapContainerRef = useRef(null);


  // initialize map when component mounts
  useEffect(() => {
    // const map = new mapboxgl.Map({
    //   container: mapContainerRef.current,
    //   // See style options here: https://docs.mapbox.com/api/maps/#styles
    //   style: 'mapbox://styles/mapbox/streets-v11',
    //   center: [-104.9876, 39.7405],
    //   zoom: 12.5,
    // });

    var map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [-87.61694, 41.86625],
      zoom: 15.99,
      // pitch: 20,
      // bearing: 20,
      antialias: true,
      pitchWithRotate: false
      });

      map.on('load', function () {
        map.addSource('floorplan', {
        // GeoJSON Data source used in vector tiles, documented at
        // https://gist.github.com/ryanbaumann/a7d970386ce59d11c16278b90dde094d
        'type': 'geojson',
        'data': 'https://docs.mapbox.com/mapbox-gl-js/assets/indoor-3d-map.geojson'
        });
        map.addLayer({
        'id': 'room-extrusion',
        'type': 'fill-extrusion',
        'source': 'floorplan',
        'paint': {
        // See the Mapbox Style Specification for details on data expressions.
        // https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions
         
        // Get the fill-extrusion-color from the source 'color' property.
        'fill-extrusion-color': ['get', 'color'],
         
        // Get fill-extrusion-height from the source 'height' property.
        // 'fill-extrusion-height': ['get', 'height'],
         
        // Get fill-extrusion-base from the source 'base_height' property.
        // 'fill-extrusion-base': ['get', 'base_height'],
         
        // Make extrusions slightly opaque for see through indoor walls.
        'fill-extrusion-opacity': 0.5
        }
        });
        });

   

    // clean up on unmount
    return () => map.remove();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return <div className="map-container" ref={mapContainerRef} />;
};

export default App;
